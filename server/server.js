const path = require('path')
const express = require('express')
const socketIO = require('socket.io')
const http = require('http')

const publicPath = path.join(__dirname,'../public')
const port = process.env.PORT||3000;
const app = express();
var server = http.createServer(app);
var io = socketIO(server)

app.use(express.static(publicPath));

io.on('connection',(socket)=>{
    console.log(`New User Connected`);
    
    // socket.emit('newEmail',{
    //     from:'sakaitbhandari@outlook.com',
    //     to:'deepaknautiyal@thinksys.com',
    //     message:'Whats going on',
    //     createAt:123
    // });

    // socket.emit('newMessage',{
    //     from:'sakaitbhandari@outlook.com',
    //     text:'Happy birthday',
    //     createAt:new Date().getTime()
    // });

    socket.emit('newMessage',{
     from:'Admin',
     text:'Welcome to the chat App',
     createdAt:new Date().getTime()
    });

    socket.broadcast.emit('newMessage',{
        from:'Admin',
        text:'New User Joined',
        createdAt:new Date().getTime()
    });

    socket.on('createEmail',(newEmail)=>{
     console.log('CreateEmail',newEmail);
    });

    socket.on('disconnect',()=>{
        console.log(`User Disconnected from server`)
    })
})


server.listen(port,()=>{
    console.log(`Server is up on port ${port}`)
})
console.log(publicPath);
